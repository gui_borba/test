/*------------------------------------------------------------------------------

 COPYRIGHT AND PROPRIETARY RIGHTS NOTICES:

 Copyright (C) 2017, an unpublished work by Syncroness, Inc.
 All rights reserved.

 This material contains the valuable properties and trade secrets of
 Syncroness, Inc. of Westminster, Colorado, United States of America
 embodying substantial creative efforts and confidential information,
 ideas and expressions, no part of which may be reproduced or
 transmitted in any form or by any means, electronic, mechanical, or
 otherwise, including photocopying and recording or in connection
 with any information storage or retrieval system, without the prior
 written permission of Syncroness, Inc.

 Project: Endeavor Endor
 File: I2C_Functions.c
 Name: I2C c file
 Purpose: contains functions to read/write FT260 I2C
 Author: Thad Briese
 Created: 04.04.2018
-----------------------------------------------------------------------------------*/
// Includes
#include "ft260_functions.h"
#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>


 /*----------------------------------------------------------------------------------*/
 // Functions

void I2C_Speed_Init (int fd)
{
  char buf[8];
  int res;

  /* Set Feature */
  buf[0] = SYSTEM_SETTING_ID;
  buf[1] = I2C_SPEED_SELECT; 
  buf[2] = 0x00;//I2C_SPEED_100KHZ_LSB;
  buf[3] = 0x00;//I2C_SPEED_100KHZ_MSB;
  //buf[2] = I2C_SPEED_1000KHZ_LSB;
  //buf[3] = I2C_SPEED_1000KHZ_MSB;
  res = ioctl(fd, HIDIOCSFEATURE(4), buf);
  if (res < 0) {
    perror("HIDIOCSFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
  }
}


void I2C_Write_Byte (int fd, char slave_addr, char reg_addr, char data)
{
  char buf[8];
  int res;
  
  // Initialize buffer with commands for I2C write and send "report" to device
  // See Section 4.5.2 of FTDI AN 394 for I2C Write Request Info
  buf[0] = I2C_WRITE_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x02; /* fixed data payload length */
  buf[4] = reg_addr;
  buf[5] = data;

  //Write values to device to initiate I2C write
  res = write(fd, buf, 6);
  if (res < 0) {
    printf("Error: %d\n", errno);
    perror("write");
  } else {
    printf("I2C_Write_Byte: write() wrote %d bytes\n", res);
  }
  usleep(1000);
}

void I2C_Burst_Write (int fd, char slave_addr, char reg_addr, char data[])
{
  char buf[64];
  int res;
  int i;
  
  // Initialize buffer with commands for I2C write and send "report" to device
  // See Section 4.5.2 of FTDI AN 394 for I2C Write Request Info
  buf[0] = I2C_WRITE_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x60; /* fixed data payload length */
  buf[4] = reg_addr;

  for (i=0;i<60;i++){
    buf[i+5] = data[i];
  }

  //Write values to device to initiate I2C write
  res = write(fd, buf, 64);
  if (res < 0) {
    printf("Error: %d\n", errno);
    perror("write");
  } else {
    printf("write() wrote %d bytes\n", res);
  }
  usleep(1000);
}

void I2C_Read_Byte (int fd, char slave_addr)
{
  char buf[8];
  int res;
  
  // Initialize buffer with commands for I2C read
  // See Section 4.5.3 of FTDI AN 394 for I2C Read Request Info
  buf[0] = I2C_READ_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x01; /* fixed data payload length LSB */
  buf[4] = 0x00; /* fixed data payload length MSB */

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);
  


  if (res < 0) {
    printf("Error: %d\n", errno);
    perror("write");
  } else {
    printf("write() wrote %d bytes\n", res);
  }
}

void I2C_Read_Word (int fd, char slave_addr)
{
  char buf[8];
  int res;
  int i;
  
  // Initialize buffer with commands for I2C read
  // See Section 4.5.3 of FTDI AN 394 for I2C Read Request Info
  buf[0] = I2C_READ_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x02; /* fixed data payload length LSB */
  buf[4] = 0x00; /* fixed data payload length MSB */

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);

  if (res < 0) {
    printf("I2C_Read_Word Error: %d\n", errno);
    perror("write");
  } else {
    printf("I2C_Read_Word: write() wrote %d bytes\n", res);
  }
  usleep(5000);
  buf[0] = 0xD1;
  buf[1] = 0x02;
  buf[2] = 0x00;
  buf[3] = 0x00; 
  buf[4] = 0x00;
  buf[5] = 0x00;
  buf[6] = 0x00;
  buf[7] = 0x00;

  res = read(fd, buf, 8);

  if (res < 0) {
    printf("I2C Read Word Error: %d\n", errno);
    perror("read");
  } else {
    printf("I2C Read Word returned: %d\n", res);
    printf("Report data:\n\t");
    for (i = 0; i< res; i++)
      printf("%hhx ", buf[i]);
    puts("\n");
  }
}

void I2C_Read_Reg2 (int fd, char slave_addr, char reg_addr)
{
  char buf[8];
  int res;
  int i;
  /*
  // Initialize buffer with commands for I2C write and send "report" to device
  // See Section 4.5.2 of FTDI AN 394 for I2C Write Request Info
  buf[0] = I2C_WRITE_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x01; // fixed payload length
  buf[4] = reg_addr;

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);
  if (res < 0) {
    printf("Error: %d\n", errno);
    perror("write");
  } else {
    printf("I2C_Read_reg: write() wrote %d bytes\n", res);
  }
  usleep(1000);
  /**/
  // Initialize buffer with commands for I2C read
  // See Section 4.5.3 of FTDI AN 394 for I2C Read Request Info
  buf[0] = I2C_READ_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x06; /* fixed data payload length LSB */
  buf[4] = 0x00; /* fixed data payload length MSB */

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);

  if (res < 0) {
    printf("I2C_Read_Word Error: %d\n", errno);
    perror("write");
  } else {
    printf("I2C_Read_Word: write() wrote %d bytes\n", res);
  }
  usleep(5000);
  buf[0] = 0xD1;
  buf[1] = 0x06;
  buf[2] = 0x00;
  buf[3] = 0x00; 
  buf[4] = 0x00;
  buf[5] = 0x00;
  buf[6] = 0x00;
  buf[7] = 0x00;

  res = read(fd, buf, 8);

  if (res < 0) {
    printf("I2C Read Word Error: %d\n", errno);
    perror("read");
  } else {
    printf("I2C Read Word returned: %d\n", res);
    printf("Report data:\n\t");
    for (i = 0; i< res; i++)
      printf("%hhx ", buf[i]);
    puts("\n");
  }
}


void I2C_Read_Reg (int fd, char slave_addr, char reg_addr)
{
  char buf[8];
  int res;
  int i;
  
  // Initialize buffer with commands for I2C write and send "report" to device
  // See Section 4.5.2 of FTDI AN 394 for I2C Write Request Info
  buf[0] = I2C_WRITE_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_START_TYPE;
  buf[3] = 0x01; // fixed payload length
  buf[4] = reg_addr;

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);
  if (res < 0) {
    printf("Error: %d\n", errno);
    perror("write");
  } else {
    //printf("I2C_Read_reg: write() wrote %d bytes\n", res);
  }
  //usleep(1000);
  
  // Initialize buffer with commands for I2C read
  // See Section 4.5.3 of FTDI AN 394 for I2C Read Request Info
  buf[0] = I2C_READ_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_REPEATED_START_TYPE;
  buf[3] = 0x05; /* fixed data payload length LSB */
  buf[4] = 0x00; /* fixed data payload length MSB */

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);

  if (res < 0) {
    printf("I2C_Read_Word Error: %d\n", errno);
    perror("write");
  } else {
    printf("I2C_Read_Word: write() wrote %d bytes\n", res);
  }
  
  buf[0] = I2C_READ_COMMAND;
  buf[1] = slave_addr;
  buf[2] = I2C_STOP_TYPE;
  buf[3] = 0x01; /* fixed data payload length LSB */
  buf[4] = 0x00; /* fixed data payload length MSB */

  //Write values to device to initiate I2C write
  res = write(fd, buf, 5);

  usleep(5000);
  buf[0] = 0xD1;
  buf[1] = 0x06;
  buf[2] = 0x00;
  buf[3] = 0x00; 
  buf[4] = 0x00;
  buf[5] = 0x00;
  buf[6] = 0x00;
  buf[7] = 0x00;

  res = read(fd, buf, 8);

  if (res < 0) {
    printf("I2C Read Word Error: %d\n", errno);
    perror("read");
  } else {
    printf("I2C Read Word returned: %d\n", res);
    printf("Report data:\n\t");
    for (i = 0; i< res; i++)
      printf("%hhx ", buf[i]);
    puts("\n");
  }
}

void I2C_Get_Status (int fd)
{
  char buf[256];
  int res;
  int i;

  i = 0;

  buf[0] = 0xC0;

  res = ioctl(fd, HIDIOCGFEATURE(256), buf);
  if (res < 0) {
    perror("HIDIOCGFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
    printf("Report data:\n\t");
    for (i = 0; i< res; i++)
      printf("%hhx ", buf[i]);
    puts("\n");
  }
}

void GPIO_Set (int fd, char gpio0_dir, char gpio0_val, char gpio1_dir, char gpio1_val)
{
  char buf[8];
  int res;

  buf[0] = GPIO_SETTING_ID;
  buf[1] = gpio0_val;
  buf[2] = gpio0_dir;
  buf[3] = gpio1_val;
  buf[4] = gpio1_dir;

  res = ioctl(fd, HIDIOCSFEATURE(6), buf);
  if (res < 0) {
    perror("HIDIOCSFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
  }
}

void GPIO_Read (int fd)
{
  char buf[256];
  int res;
  int i;

  i = 0;

  buf[0] = 0xB0;//SYSTEM_SETTING_ID;

  res = ioctl(fd, HIDIOCGFEATURE(256), buf);
  if (res < 0) {
    perror("HIDIOCGFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
    printf("Report data:\n\t");
    for (i = 0; i< res; i++)
      printf("%hhx ", buf[i]);
    puts("\n");
  }
}

void GPIO2_Enable (int fd)
{
  char buf[8];
  int res;

  buf[0] = 0xA1;//SYSTEM_SETTING_ID;
  buf[1] = 0x06;
  buf[2] = 0x00;

  res = ioctl(fd, HIDIOCSFEATURE(3), buf);
  if (res < 0) {
    perror("HIDIOCSFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
  }
}

void GPIO3_Enable (int fd)
{
  char buf[8];
  int res;

  buf[0] = 0xA1;//SYSTEM_SETTING_ID;
  buf[1] = 0x05;
  buf[2] = 0x00;

  res = ioctl(fd, HIDIOCSFEATURE(3), buf);
  if (res < 0) {
    perror("HIDIOCSFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
  }
}

void GPIOA_Enable (int fd)
{
  char buf[8];
  int res;
  
  buf[0] = 0xA1;//SYSTEM_SETTING_ID;
  buf[1] = 0x08;
  buf[2] = 0x00;

  res = ioctl(fd, HIDIOCSFEATURE(3), buf);
  if (res < 0) {
    perror("HIDIOCSFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
  }
}

void GPIOG_Enable (int fd)
{
  char buf[8];
  int res;
  
  buf[0] = 0xA1;//SYSTEM_SETTING_ID;
  buf[1] = 0x09;
  buf[2] = 0x00;

  res = ioctl(fd, HIDIOCSFEATURE(3), buf);
  if (res < 0) {
    perror("HIDIOCSFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
  }
}

void Status_Get (int fd)
{
  char buf[256];
  int res;
  int i;

  i = 0;

  buf[0] = 0xA1;//SYSTEM_SETTING_ID;

  res = ioctl(fd, HIDIOCGFEATURE(256), buf);
  if (res < 0) {
    perror("HIDIOCGFEATURE");
  } else {
    printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
    printf("Report data:\n\t");
    for (i = 0; i< res; i++)
      printf("%hhx ", buf[i]);
    puts("\n");
  }
}

void LED_Brightness (int fd, char level)
{
  char buf[8];
  int res;
  
  buf[0] = I2C_WRITE_COMMAND;
  buf[1] = 0x2F; // pot address
  buf[2] = I2C_START_AND_STOP_TYPE;
  buf[3] = 0x02; /* fixed data payload length */
  
  if (level = 0) {
    buf[4] = 0x05;
    buf[5] = 0x21;
  } else if (level = 1) {
    buf[4] = 0x04;
    buf[5] = 0xB5;
  } else if (level = 2) {
    buf[4] = 0x04;
    buf[5] = 0x80;
  } else if (level = 3) {
    buf[4] = 0x04;
    buf[5] = 0x5F;
  } else if (level = 4) {
	buf[4] = 0x04;
    buf[5] = 0x4A;
  } else if (level = 5) {
	buf[4] = 0x04;
    buf[5] = 0x3B;
  } else if (level = 6) {
	buf[4] = 0x04;
    buf[5] = 0x30;
  } else if (level = 7) {
    buf[4] = 0x04;
    buf[5] = 0x27;
  } else if (level = 8) {
    buf[4] = 0x04;
    buf[5] = 0x20;
  } else if (level = 9) {
    buf[4] = 0x04;
    buf[5] = 0x1A;
  } else if (level = 10) {
    buf[4] = 0x04;
    buf[5] = 0x15;
  } else if (level = 11) {
    buf[4] = 0x04;
    buf[5] = 0x11;
  } else {
    buf[4] = 0x04;
    buf[5] = 0x0D;
    printf("Maximum Brightness\n");
  }

  //Write values to device to initiate I2C write
  res = write(fd, buf, 6);
  if (res < 0) {
    printf("Error: %d\n", errno);
    perror("write");
  } else {
    printf("write() wrote %d bytes\n", res);
  }
  usleep(1000);
}
