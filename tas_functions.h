/*---------------------------------------------------------------------------------

   COPYRIGHT AND PROPRIETARY RIGHTS NOTICES:

   Copyright (C) 2015, an unpublished work by Syncroness, Inc.
   All rights reserved.

   This material contains the valuable properties and trade secrets of
   Syncroness, Inc. of Westminster, Colorado, United States of America
   embodying substantial creative efforts and confidential information,
   ideas and expressions, no part of which may be reproduced or
   transmitted in any form or by any means, electronic, mechanical, or
   otherwise, including photocopying and recording or in connection
   with any information storage or retrieval system, without the prior
   written permission of Syncroness, Inc.

   Project:
   File:    FT260_Functions.h
   Name:    FT260 Functions header file
   Purpose: contains functions, #defines and type definitions for I2C Functions c file

   Author: Thad Briese
   Created: 04.04.17
------------------------------------------------------------------------------------*/
#ifndef _TAS_FUNCTIONS_H
#define _TAS_FUNCTIONS_H

/*----------------------------------------------------------------------------------*/
// #Defines
#define TAS2557_ADDR       0x4C

/*----------------------------------------------------------------------------------*/
// Function Prototypes
/*----------------------------------------------------------------------------------*/
// Function Name: TAS_Init()
// Function Purpose/Theory:
//    Initialize smart amp
// Global Variables Used: none
// Inputs: Device handle
// Outputs: none
//


void TAS_Init (int fd);

#endif // _TAS_FUNCTIONS_H

