/*---------------------------------------------------------------------------------

   COPYRIGHT AND PROPRIETARY RIGHTS NOTICES:

   Copyright (C) 2015, an unpublished work by Syncroness, Inc.
   All rights reserved.

   This material contains the valuable properties and trade secrets of
   Syncroness, Inc. of Westminster, Colorado, United States of America
   embodying substantial creative efforts and confidential information,
   ideas and expressions, no part of which may be reproduced or
   transmitted in any form or by any means, electronic, mechanical, or
   otherwise, including photocopying and recording or in connection
   with any information storage or retrieval system, without the prior
   written permission of Syncroness, Inc.

   Project:
   File:    FT260_Functions.h
   Name:    FT260 Functions header file
   Purpose: contains functions, #defines and type definitions for I2C Functions c file

   Author: Thad Briese
   Created: 04.04.17
------------------------------------------------------------------------------------*/
#ifndef _FT260_FUNCTIONS_H
#define _FT260_FUNCTIONS_H

/*----------------------------------------------------------------------------------*/
// #Defines
#define SYSTEM_SETTING_ID       0xA1
#define I2C_SPEED_SELECT        0x22
#define I2C_SPEED_1000KHZ_LSB   0xE8
#define I2C_SPEED_1000KHZ_MSB   0x03
#define I2C_SPEED_400KHZ_LSB    0x90
#define I2C_SPEED_400KHZ_MSB    0x01
#define I2C_SPEED_100KHZ_LSB    0x01
#define I2C_SPEED_100KHZ_MSB    0x01
#define I2C_WRITE_COMMAND       0xD0
#define I2C_READ_COMMAND        0xC2
#define I2C_START_TYPE          0x02
#define I2C_REPEATED_START_TYPE 0x03
#define I2C_STOP_TYPE           0x04
#define I2C_START_AND_STOP_TYPE 0x06
#define GPIO_SETTING_ID		0xB0

/*----------------------------------------------------------------------------------*/
// Function Prototypes
/*----------------------------------------------------------------------------------*/
// Function Name: I2C_Init()
// Function Purpose/Theory:
//    Initialize I2C Speed
// Global Variables Used: none
// Inputs: Device handle
// Outputs: none
//
void I2C_Speed_Init (int fd);

// Function Name: I2C_Write_Byte()
// Function Purpose/Theory:
//    Writes 1 byte to a register address over I2C
// Global Variables Used: none
// Inputs: device handle, addresses, data to write
// Outputs: none
//
void I2C_Write_Byte (int fd, char slave_addr, char reg_addr, char data);

void I2C_Burst_Write (int fd, char slave_addr, char reg_addr, char data[]);

// Function Name: I2C_Read_Byte()
// Function Purpose/Theory:
//    Sends command to read 1 byte via I2C
// Global Variables Used: none
// Inputs: device handle, address
// Outputs: none
//
void I2C_Read_Byte (int fd, char slave_addr);

// Function Name: I2C_Read_Word()
// Function Purpose/Theory:
//    Sends command to read 2 bytes via I2C
// Global Variables Used: none
// Inputs: device handle, address
// Outputs: none
//
void I2C_Read_Word (int fd, char slave_addr);

void I2C_Read_Reg (int fd, char slave_addr, char reg_addr);
void I2C_Read_Reg2 (int fd, char slave_addr, char reg_addr);

// Function Name: I2C_Get_Status()
// Function Purpose/Theory:
//    Reads I2C status register
// Global Variables Used: none
// Inputs: device handle
// Outputs: none
//
void I2C_Get_Status (int fd);

// Function Name: GPIO_Set()
// Function Purpose/Theory:
//    Sets the GPIO direction and value
// Global Variables Used: none
// Inputs: device handle, direction for GPIO0-5, value for GPIO0-5, direction for GPIOA-H,
//    value for GPIOA-H
// Outputs: none
//
void GPIO_Set (int fd, char gpio0_dir, char gpio0_val, char gpio1_dir, char gpio1_val);

// Function Name: GPIO_Read()
// Function Purpose/Theory:
//    Reads and prints current GPIO direction and value
// Global Variables Used: none
// Inputs: device handle
// Outputs: none
//
void GPIO_Read (int fd);

// Function Name: Status_Get()
// Function Purpose/Theory:
//    Reads status register to see current DIO pin states and more
// Global Variables Used: none
// Inputs: device handle
// Outputs: none
//
void Status_Get (int fd);

// Function Name: GPIOX_Enable()
// Function Purpose/Theory:
//    Configures pin as GPIO
// Global Variables Used: none
// Inputs: device handle
// Outputs: none
//
void GPIO2_Enable (int fd);
void GPIO3_Enable (int fd);
void GPIOA_Enable (int fd);
void GPIOG_Enable (int fd);

void LED_Brightness (int fd, char level);

#endif // _FT260_FUNCTIONS_H

