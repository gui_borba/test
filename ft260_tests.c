#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include "ft260_functions.h"
#include "tas_functions.h"

/*
* For the systems that don't have the new version of hidraw.h in
userspace.
*/

#ifndef HIDIOCSFEATURE
#warning Please have your distro update the userspace kernel headers
#define HIDIOCSFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x06, len)
#define HIDIOCGFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x07, len)
#endif


// Syncroness defines
//#define TEST_LM3414       1
#define TEST_TLV320ADC    1
#define TEST_TAS2557      1

#define AD5272_ADDR       0x2F
#define TLV320ADC_ADDR    0x18 
#define TAS2557_ADDR      0x4C

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

const char*
bus_type_str(int bus)
{
  switch (bus)
  {
    case BUS_USB:       return "USB";
    case BUS_HIL:       return "HIL";
    case BUS_BLUETOOTH: return "Bluetooth";
    case BUS_VIRTUAL:   return "Virtual";
    default: return "Other";
  }
}

void test_lm3414(int fd)
{
  int i;

  // Enable writes to AD5272, command 7 is 0x1C (b00_01_11_00)
  I2C_Write_Byte (fd, AD5272_ADDR, 0x1C, 0x02);
  
  
  for (i = 0; i < 10; i++)
  {
    // Write intensity to AD5272 - 0x9A is about 3kohm, command 1 is 0x04 (b00_00_01_XX)
    I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x9A);

    sleep(1);

    // Write intensity to AD5272 - 0x3FF is 20kohm, command 1 with 3FF for data
    I2C_Write_Byte (fd, AD5272_ADDR, 0x07, 0xFF);

    sleep(1);

  }
}

void test_tlv320adc(int fd)
{
  
  // Set register page to 0
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x00, 0x00);

  // Initialize SW reset
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x01, 0x01);


  /* ----------------------------------------
     Program clock settings
  -------------------------------------------*/

  // Program multiplexing
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x04, 0x03); // PLL_CLKIN = MCLK, CODEC_CLKIN = PLL_CLK
  // Program P VAL = 1 and R VAL = 1
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x05, 0x11);
  // Program J VAL = 8
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x06, 0x08);
  // Program D VAL = 1920 (0x780)
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x07, 0x07);
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x08, 0x80);

  // Power up PLL
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x05, 0x91);

  // Program and Power up NADC, NADC = 8
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x12, 0x88);
  // Program and Power up MADC, MADC = 2
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x13, 0x82);

  // Program OSR value (128)
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x14, 0x80);

  // Program BCLK mux 
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1D, 0x02);

  // Program BCLK_DIV value (8) and power up
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1E, 0x84);

  // Program I2S settings, I2S, outputs for WCLK, BCLK, sample size
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1B, 0x3C); // 16-bit
  //I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1B, 0x2C); // 24-bit

  // Program the processing block to be used
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x3D, 0x01);

  /* ----------------------------------------
     Program analog blocks
  -------------------------------------------*/
  // Set register page to 1.
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x00, 0x01);

  // Program MICBIAS (if applicable)
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x33, 0x00); // default, not used

  // Program PGAs to 0dB
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x3B, 0x00); // LEFT
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x3C, 0x00); // RIGHT
 
  /* ----------------------------------------
     Routing of signals to ADC inputs
  -------------------------------------------*/
  // Route left input IN1L(P) to left PGA
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x34, 0xFC);
  // Route right input IN1R(M) to right PGA
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x37, 0xFC);

  /* ----------------------------------------
     Program ADC
  -------------------------------------------*/
  // Set register page to 0.
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x00, 0x00);

  // Power up left ADC and right ADC
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x51, 0xC2);

  // Unmute digital volume control and set gain = 0dB
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x52, 0x00);

  // 
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x53, 0x28);

  // 
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x54, 0x28);

}


int main(int argc, char **argv)
{
  int fd;
  int res, desc_size = 0;
  char buf[256];
  struct hidraw_report_descriptor rpt_desc;
  struct hidraw_devinfo info;
  char* device = "/dev/hidraw1";
  if (argc > 1) {
    device = argv[1];
  }



  /* Open the Device with non-blocking reads. */
  /* It will be better if use libudev instead of hard coded path. 
  You can check Appendix A for the example of using libudev */
  fd = open(device, O_RDWR|O_NONBLOCK);
  if (fd < 0) {
    perror("Unable to open device");
    return 1;
  }

  memset(&rpt_desc, 0x0, sizeof(rpt_desc));
  memset(&info, 0x0, sizeof(info));
  memset(buf, 0x0, sizeof(buf));

  /* Get Report Descriptor Size */
  res = ioctl(fd, HIDIOCGRDESCSIZE, &desc_size);
  if (res < 0) {
    perror("HIDIOCGRDESCSIZE");
  } else {
    printf("Report Descriptor Size: %d\n", desc_size);
  }

  /* Get Raw Name */
  res = ioctl(fd, HIDIOCGRAWNAME(256), buf);
  if (res < 0) {
    perror("HIDIOCGRAWNAME");
  } else {
    printf("Raw Name: %s\n", buf);
  }

  /* Get Raw Info */
  res = ioctl(fd, HIDIOCGRAWINFO, &info);
  if (res < 0) {
    perror("HIDIOCGRAWINFO");
  } else {
    printf("Raw Info:\n");
    printf("\tbustype: %d (%s)\n",info.bustype, bus_type_str(info.bustype));
    printf("\tvendor: 0x%04hx\n", info.vendor);
    printf("\tproduct: 0x%04hx\n", info.product);
  }

  I2C_Speed_Init (fd);
  GPIO_Set (fd, 0x30, 0x30, 0x00, 0xA2);

  #ifdef TEST_LM3414
    test_lm3414 (fd);
  #endif

  #ifdef TEST_TLV320ADC
    test_tlv320adc (fd);
  #endif
  usleep(50000);
  #ifdef TEST_TAS2557
    TAS_Init (fd);
    //amp_reset (fd);
  #endif
    
  
  close(fd);
  return 0;
}


