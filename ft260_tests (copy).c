#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include "I2C_Functions.h"

/*
* For the systems that don't have the new version of hidraw.h in
userspace.
*/

#ifndef HIDIOCSFEATURE
#warning Please have your distro update the userspace kernel headers
#define HIDIOCSFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x06, len)
#define HIDIOCGFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x07, len)
#endif


// Syncroness defines
//#define TEST_LM3414       1
//#define TEST_TLV320ADC    1
#define TEST_TAS2557      1

#define AD5272_ADDR       0x2E
#define TLV320ADC_ADDR    0x18 
#define TAS2557_ADDR      0x4C

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

const char*
bus_type_str(int bus)
{
  switch (bus)
  {
    case BUS_USB:       return "USB";
    case BUS_HIL:       return "HIL";
    case BUS_BLUETOOTH: return "Bluetooth";
    case BUS_VIRTUAL:   return "Virtual";
    default: return "Other";
  }
}

void test_lm3414(int fd)
{
  int i;

  // Enable writes to AD5272, command 7 is 0x1C (b00_01_11_00)
  I2C_Write_Byte (fd, AD5272_ADDR, 0x1C, 0x02);
  
  
  for (i = 0; i < 10; i++)
  {
    // Write intensity to AD5272 - 0x9A is about 3kohm, command 1 is 0x04 (b00_00_01_XX)
    I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x9A);

    sleep(1);

    // Write intensity to AD5272 - 0x3FF is 20kohm, command 1 with 3FF for data
    I2C_Write_Byte (fd, AD5272_ADDR, 0x07, 0xFF);

    sleep(1);

  }
}

void test_tlv320adc(int fd)
{
  
  // Set register page to 0
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x00, 0x00);

  // Initialize SW reset
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x01, 0x01);


  /* ----------------------------------------
     Program clock settings
  -------------------------------------------*/

  // Program multiplexing
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x04, 0x03); // PLL_CLKIN = MCLK, CODEC_CLKIN = PLL_CLK
  // Program P VAL = 1 and R VAL = 1
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x05, 0x11);
  // Program J VAL = 8
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x06, 0x08);
  // Program D VAL = 1920 (0x780)
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x07, 0x07);
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x08, 0x80);

  // Power up PLL
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x05, 0x91);

  // Program and Power up NADC, NADC = 8
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x12, 0x88);
  // Program and Power up MADC, MADC = 2
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x13, 0x82);

  // Program OSR value (128)
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x14, 0x80);

  // Program BCLK mux 
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1D, 0x02);

  // Program BCLK_DIV value (4) and power up
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1E, 0x84);

  // Program I2S settings, I2S, outputs for WCLK, BCLK, sample size
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1B, 0x3C); // 32-bit
  //I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x1B, 0x2C); // 24-bit

  // Program the processing block to be used
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x3D, 0x01);

  /* ----------------------------------------
     Program analog blocks
  -------------------------------------------*/
  // Set register page to 1.
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x00, 0x01);

  // Program MICBIAS (if applicable)
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x33, 0x00); // default, not used

  // Program PGAs to 0dB
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x3B, 0x00); // LEFT
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x3C, 0x00); // RIGHT
 
  /* ----------------------------------------
     Routing of signals to ADC inputs
  -------------------------------------------*/
  // Route left input IN1L(P) to left PGA
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x34, 0xFC);
  // Route right input IN1R(M) to right PGA
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x37, 0xFC);

  /* ----------------------------------------
     Program ADC
  -------------------------------------------*/
  // Set register page to 0.
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x00, 0x00);

  // Power up left ADC and right ADC
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x51, 0xC2);

  // Unmute digital volume control and set gain = 0dB
  I2C_Write_Byte (fd, TLV320ADC_ADDR, 0x52, 0x00);

}


void test_tas2557(int fd)
{

  usleep(100);  // wait 100 usec
  /* ----------------------------------------
     Init device
  -------------------------------------------*/

  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x64);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x07, 0x01);
  usleep(1000);  // wait 1000 usec
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x07, 0x03);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2c, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x04, 0x60);
  usleep(200);  // wait 200 usec
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x05, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x04, 0x00);


  /* ----------------------------------------
     SW Reset
  -------------------------------------------*/
  // Set register page to 0
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);

  // Set book to 0
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7F, 0x00);

  // Initialize SW reset
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x01, 0x01);

  usleep(100);  // wait 100 usec

  /* ----------------------------------------
     PLL Config
  -------------------------------------------*/
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x01);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x73, 0x0f);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x74, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x64);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x1b, 0x01);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x1c, 0x20);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x1d, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x1e, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x20, 0x02);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x22, 0x08);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x02, 0x10);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x21, 0x08);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x01, 0x08);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2b, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2c, 0x40);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x1f, 0x20);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2a, 0x00);

  // Set data size to 16 bits
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x01);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x01, 0x00);

  /* ----------------------------------------
     Program
  -------------------------------------------*/
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0xfd);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x0d, 0x0d);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0xfe);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x28, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x08, 0x02);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x0a, 0x08);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x34, 0x03);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x32);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x3c, 0x3d);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x70, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x68, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x33);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x64, 0x39);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x35);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x4c, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x34);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x68, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x6c, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x70, 0x7f);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x22, 0x02);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x82);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x19);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x38, 0xd1);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x32);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x3c, 0x3d);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x4c, 0x02);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x60, 0x28);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x33);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x40, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x64, 0x39);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x06, 0x7c);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2b, 0x03);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2a, 0x04);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x64);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x28, 0x80);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x08, 0x02);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x33);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x10, 0x73);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x20, 0x1d);
  /* ----------------------------------------
     PRE
  -------------------------------------------*/
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2f, 0x2a);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2d, 0x17);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x02);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x06, 0x01);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x32);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x54, 0x00);

  /* ----------------------------------------
     ???
  -------------------------------------------*/
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x05, 0xa0);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x05, 0xa3);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x04, 0xfa);
  usleep(200);  // wait 200 usec
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2d, 0x21);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2e, 0x21);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x2c, 0x0b);
  usleep(200);  // wait 200 usec
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x00, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x07, 0x00);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x7f, 0x64);
  I2C_Write_Byte (fd, TAS2557_ADDR, 0x07, 0x00);

}



int main(int argc, char **argv)
{
  int fd;
  int res, desc_size = 0;
  char buf[256];
  struct hidraw_report_descriptor rpt_desc;
  struct hidraw_devinfo info;
  char* device = "/dev/hidraw1";
  if (argc > 1) {
    device = argv[1];
  }



  /* Open the Device with non-blocking reads. */
  /* It will be better if use libudev instead of hard coded path. 
  You can check Appendix A for the example of using libudev */
  fd = open(device, O_RDWR|O_NONBLOCK);
  if (fd < 0) {
    perror("Unable to open device");
    return 1;
  }

  memset(&rpt_desc, 0x0, sizeof(rpt_desc));
  memset(&info, 0x0, sizeof(info));
  memset(buf, 0x0, sizeof(buf));

  /* Get Report Descriptor Size */
  res = ioctl(fd, HIDIOCGRDESCSIZE, &desc_size);
  if (res < 0) {
    perror("HIDIOCGRDESCSIZE");
  } else {
    printf("Report Descriptor Size: %d\n", desc_size);
  }

  /* Get Raw Name */
  res = ioctl(fd, HIDIOCGRAWNAME(256), buf);
  if (res < 0) {
    perror("HIDIOCGRAWNAME");
  } else {
    printf("Raw Name: %s\n", buf);
  }

  /* Get Raw Info */
  res = ioctl(fd, HIDIOCGRAWINFO, &info);
  if (res < 0) {
    perror("HIDIOCGRAWINFO");
  } else {
    printf("Raw Info:\n");
    printf("\tbustype: %d (%s)\n",info.bustype, bus_type_str(info.bustype));
    printf("\tvendor: 0x%04hx\n", info.vendor);
    printf("\tproduct: 0x%04hx\n", info.product);
  }

  I2C_Speed_Init (fd);

  #ifdef TEST_LM3414
    test_lm3414 (fd);
  #endif

  #ifdef TEST_TLV320ADC
    test_tlv320adc (fd);
  #endif

  #ifdef TEST_TAS2557
    test_tas2557 (fd);
  #endif
    
  
  close(fd);
  return 0;
}


