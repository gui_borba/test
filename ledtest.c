#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include "FT260_Functions.h"

/*
* For the systems that don't have the new version of hidraw.h in
userspace.
*/

#ifndef HIDIOCSFEATURE
#warning Please have your distro update the userspace kernel headers
#define HIDIOCSFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x06, len)
#define HIDIOCGFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x07, len)
#endif


#define AD5272_ADDR       0x2F
#define TLV320ADC_ADDR    0x18 
#define TAS2557_ADDR      0x4C

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

const char*
bus_type_str(int bus)
{
  switch (bus)
  {
    case BUS_USB:       return "USB";
    case BUS_HIL:       return "HIL";
    case BUS_BLUETOOTH: return "Bluetooth";
    case BUS_VIRTUAL:   return "Virtual";
    default: return "Other";
  }
}

int main(int argc, char **argv)
{
  int fd;
  int res, desc_size = 0;
  char buf[256];
  struct hidraw_report_descriptor rpt_desc;
  struct hidraw_devinfo info;
  char* device = "/dev/hidraw1";
  if (argc > 1) {
    device = argv[1];
  }

  /* Open the Device with non-blocking reads. */
  /* It will be better if use libudev instead of hard coded path. 
  You can check Appendix A for the example of using libudev */
  fd = open(device, O_RDWR|O_NONBLOCK);
  if (fd < 0) {
    perror("Unable to open device");
    return 1;
  }

  memset(&rpt_desc, 0x0, sizeof(rpt_desc));
  memset(&info, 0x0, sizeof(info));
  memset(buf, 0x0, sizeof(buf));

  /* Get Report Descriptor Size */
  res = ioctl(fd, HIDIOCGRDESCSIZE, &desc_size);
  if (res < 0) {
    perror("HIDIOCGRDESCSIZE");
  } else {
    printf("Report Descriptor Size: %d\n", desc_size);
  }

  /* Get Raw Name */
  res = ioctl(fd, HIDIOCGRAWNAME(256), buf);
  if (res < 0) {
    perror("HIDIOCGRAWNAME");
  } else {
    printf("Raw Name: %s\n", buf);
  }

  /* Get Raw Info */
  res = ioctl(fd, HIDIOCGRAWINFO, &info);
  if (res < 0) {
    perror("HIDIOCGRAWINFO");
  } else {
    printf("Raw Info:\n");
    printf("\tbustype: %d (%s)\n",info.bustype, bus_type_str(info.bustype));
    printf("\tvendor: 0x%04hx\n", info.vendor);
    printf("\tproduct: 0x%04hx\n", info.product);
  }

  I2C_Speed_Init (fd);
  GPIO3_Enable (fd);
  GPIO_Set (fd, 0x00, 0x00, 0xFF, 0xA2);
  //GPIO_Set (fd, 0xFF, 0x08, 0xFF, 0xA0);
  GPIO_Read (fd);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x1C, 0x02);
  //LED_Brightness (fd, 13);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x0D);
  //I2C_Write_Byte (fd, AD5272_ADDR, 0x06, 0xFF);
  usleep(5000);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x08, 0x00);
  sleep(1);
  I2C_Read_Word (fd, AD5272_ADDR);
  /*sleep(2);
  GPIO_Set (fd, 0x00, 0x00, 0xFF, 0x82);
  GPIO_Read (fd);
  sleep(2);
  GPIO_Set (fd, 0x00, 0x08, 0xFF, 0xA2);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x00);
  GPIO_Read (fd);
  /*sleep(2);
  GPIO_Set (fd, 0x00, 0x00, 0xFF, 0x82);
  GPIO_Read (fd);
  sleep(2);
  GPIO_Set (fd, 0x00, 0x08, 0xFF, 0xA2);
  GPIO_Read (fd);
  sleep(2);
  LED_Brightness (fd, 5);
  GPIO_Set (fd, 0x00, 0x00, 0xFF, 0x82);
  GPIO_Read (fd);
  sleep(2);
  GPIO_Set (fd, 0x00, 0x08, 0xFF, 0xA2);
  GPIO_Read (fd);
  sleep(2);
  GPIO_Set (fd, 0x00, 0x00, 0xFF, 0x82);
  GPIO_Read (fd);
  sleep(2);
  GPIO_Set (fd, 0x00, 0x08, 0xFF, 0xA2);
  GPIO_Read (fd);
  sleep(2);
  //GPIO_Set (fd, 0x00, 0x00, 0xFF, 0x00);
  GPIO_Read (fd);
  sleep(2);
  /**/
  
  
  /*I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x00);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x07, 0xFF);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x00);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x07, 0xFF);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x00);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x07, 0xFF);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x04, 0x00);
  sleep(3);
  I2C_Write_Byte (fd, AD5272_ADDR, 0x07, 0xFF);
  sleep(3);
  /**/
  
  close(fd);
  return 0;
}


