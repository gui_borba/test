#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
/*
* For the systems that don't have the new version of hidraw.h in
userspace.
*/
#ifndef HIDIOCSFEATURE
#warning Please have your distro update the userspace kernel headers
#define HIDIOCSFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x06, len)
#define HIDIOCGFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x07, len)
#endif
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
const char*
bus_type_str(int bus)
{
switch (bus)
{
case BUS_USB:       return "USB";
case BUS_HIL:       return "HIL";
case BUS_BLUETOOTH: return "Bluetooth";
case BUS_VIRTUAL:   return "Virtual";
default: return "Other";
}
}
int main(int argc, char **argv)
{
int fd;
int res, desc_size = 0;
char buf[256];
struct hidraw_report_descriptor rpt_desc;
struct hidraw_devinfo info;
char* device = "/dev/hidraw2";
if (argc > 1) {
device = argv[1];
}



/* Open the Device with non-blocking reads. */
/* It will be better if use libudev instead of hard coded path. 
You can check Appendix A for the example of using libudev */
fd = open(device, O_RDWR|O_NONBLOCK);
if (fd < 0) {
perror("Unable to open device");
return 1;
}
memset(&rpt_desc, 0x0, sizeof(rpt_desc));
memset(&info, 0x0, sizeof(info));
memset(buf, 0x0, sizeof(buf));
/* Get Report Descriptor Size */
res = ioctl(fd, HIDIOCGRDESCSIZE, &desc_size);
if (res < 0) {
perror("HIDIOCGRDESCSIZE");
} else {
printf("Report Descriptor Size: %d\n", desc_size);
}
/* Get Raw Name */
res = ioctl(fd, HIDIOCGRAWNAME(256), buf);
if (res < 0) {
perror("HIDIOCGRAWNAME");
} else {
printf("Raw Name: %s\n", buf);
}
/* Get Raw Info */
res = ioctl(fd, HIDIOCGRAWINFO, &info);
if (res < 0) {
perror("HIDIOCGRAWINFO");
} else {
printf("Raw Info:\n");
printf("\tbustype: %d (%s)\n",info.bustype, bus_type_str(info.bustype));
printf("\tvendor: 0x%04hx\n", info.vendor);
printf("\tproduct: 0x%04hx\n", info.product);
}
/* Set Feature */
buf[0] = 0xA1; /* SYSTEM_SETTING_ID */
buf[1] = 0x22; /* 
I2C_SPEED */
buf[2] = 0x01; /* 400Kbps */
buf[3] = 0x90;
res = ioctl(fd, HIDIOCSFEATURE(4), buf);
if (res < 0) {
perror("HIDIOCSFEATURE");
} else {
printf("ioctl HIDIOCGFEATURE returned: %d\n", res);
}
/* Send a Report to the Device */
buf[0] = 0xD0; /* I2C write */
buf[1] = 0x22; /* Slave address */
buf[2] = 0x06; /* Start and Stop */
buf[3] = 0x03; /* data len */



buf[4] = 'a';
buf[5] = 'b';
buf[6] = 'c';
res = write(fd, buf, 7);
if (res < 0) {
printf("Error: %d\n", errno);
perror("write");
} else {
printf("write() wrote %d bytes\n", res);
}
close(fd);
return 0;
}
